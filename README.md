# DecimalQtyInvoiceFix

Fix for invoicing decimal quantities in Magento 2.

Based on [this issue](https://github.com/magento/magento2/issues/12986).

Somehow Magento 2 rounds product quantities on invoice page when items has decimal quantities.

Magento Core says it solved on 2.3 but it has not been released so we have to try to solve by on our own and we did.

We hope this repo will be helped someone :)

## Install

    composer require irm/module-decimalqtyinvoicefix:dev-master
    bin/magento module:enable IRM_DecimalQtyInvoiceFix
    bin/magento setup:upgrade
    bin/magento setup:di:compile
    bin/magento cache:flush