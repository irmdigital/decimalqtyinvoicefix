<?php

namespace IRM\DecimalQtyInvoiceFix\Plugin;

class AroundPrepareInvoice
{
  protected $resourceConnection;
  protected $connection;
  protected $tableName;
  protected $logger;

  function __construct(
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    \Psr\Log\LoggerInterface $logger
  )
  {
    $this->logger = $logger;
    $this->resourceConnection = $resourceConnection;
    $this->connection = $this->resourceConnection->getConnection();
    $this->tableName = $this->resourceConnection->getTableName('cataloginventory_stock_item');
  }

  public function aroundPrepareInvoice(\Magento\Sales\Model\Service\InvoiceService $subject, callable $proceed, \Magento\Sales\Model\Order $order, array $qtys = [])
  {
    foreach ($order->getAllItems() as $orderItem) {
      if($this->isQtyDecimal($orderItem->getProduct()->getId())) {
        $orderItem->setIsQtyDecimal(1);
      }
    }

    $result = $proceed($order, $qtys);
    return $result;
  }

  public function isQtyDecimal($productId){
    $sql = sprintf("SELECT is_qty_decimal FROM " . $this->tableName . " WHERE product_id='%d'", $productId);
    $result = $this->connection->fetchRow($sql);

    // You can uncomment following line for checking query result, if you want
    // $this->logger->info("Is decimal qty result for product_id={$productId}: " . http_build_query($result)); // look at {magento_dir}/var/log/system.log

    return (bool) $result['is_qty_decimal'];
  }
}